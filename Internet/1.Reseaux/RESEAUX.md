# Activité - Réseaux 

## Tous et toutes connectées

Un groupe de neuf amies souhaite connecter leurs ordinateurs ensemble pour jouer. Elles les connectent en utilisant un réseau filaire (en utilisant des câbles).

Elles s'interrogent sur le nombre de câbles et la sécurité de leurs futur réseau, et étudient plusieurs schémas de connexion possibles. Plus précisément, leurs interrogations sont :

- Comment utiliser le moins de câbles possibles ?
- Comment faire pour que si un des ordinateur tombe en panne, ou un des câbles est coupé, les autres ordinateurs puissent toujours communiquer ensemble ?
- Comment faire pour éviter qu'une des joueuses puisse tricher en écoutant les communications entre les autres joueuses ?
- Comment faire pour éviter qu'une des joueuses puisse tricher en bloquant les communications entre les autres joueuses ?

Nous allons essayer de trouver la meilleure configuration possible (sachant qu'il n'existe pas de configuration parfaite).

### 1. Réseau centralisé

La première configuration que les joueuses essayent est centralisée : l'ordinateur de la joueuse A est au centre de la pièce, et chacun des autres ordinateurs est relié à l'ordinateur A, comme dans l'illustration suivante (à cinq ordinateurs, où quatre câbles sont utilisés). Par exemple, pour communiquer ensemble, les ordinateurs B et D vont envoyer un message passant par l'ordinateur A.

```mermaid
graph LR
    B --- A
    C --- A
    A --- E
    A --- D
```

#### Questions

1. ✏️ Faire un schéma du réseau à 9 ordinateurs (A, B, C, D, E, F, G, H, I) : l'ordinateur A est toujours au centre, et chacun des autres ordinateurs est relié directement à lui par un seul câble.
2. ✏️ Combien de câbles sont alors nécessaires pour 9 ordinateurs ?
3. ✏️ Généraliser pour $n$ ordinateurs

####  Remarques

On remarque deux choses :

- Si un câble est coupé (par exemple celui entre A et B), l'ordinateur B est exclu et ne peut plus communiquer avec les autres.
- Toutes les communications passent par l'ordinateur A. Donc en cas de panne de l'ordinateur A, plus aucune communication n'est possible.
- Toutes les communications passent par l'ordinateur A. Celui-ci peut donc tricher en interdisant à des ordinateurs de communiquer entre eux, ou en espionnant les communications entre les autres ordinateurs.

Nous allons essayer de corriger cela dans les autres configurations.

### 2. Réseau décentralisé

Les amies essayent alors de « décentraliser » le réseau, comme dans le schéma suivant.

```mermaid
graph LR
    H --- C
    G --- C
    C --- A
    A --- B
    A --- D
    D --- I
    B --- F
    B --- E
```

#### Questions

1. ✏️ Combien de câbles sont nécessaires ?
2. ✏️ Un ordinateur peut-il être exclu du réseau en cas de coupure d'un câble ?
3. Triche
   1. ✏️ Par où passe un message allant de E à C ?
   2. ✏️ Par où passe un message allant de G à H ?
   3. ✏️ Toutes les communications passent-elles encore par le même ordinateur ? Justifier.
4. Panne
   1. ✏️ Donnez un exemple d'ordinateur qui, s'il tombe en panne, empêche d'autres ordinateurs de communiquer.
   2. ✏️ Donnez un exemple d'ordinateur qui, s'il tombe en panne, n'affecte pas les autres ordinateurs.

### 3. Graphe complet

Les joueuses décident maintenant que chaque ordinateur est connecté directement à chacun des autres, comme dans l'exemple suivant (à cinq ordinateurs).

```mermaid
graph LR
	A --- B
	A --- C
	A --- D
	A --- E
	B --- C
	B --- D
	B --- E
	C --- D
	C --- E
	D --- E
```

Par exemple, en message allant de A à B peut soit emprunter directement le câble entre A et B, soit « faire un détour » en passant par C.

#### Questions

1. ✏️ Faire un schéma du réseau à 9 ordinateurs (A, B, C, D, E, F, G, H, I), où chaque ordinateur est relié directement par un câble à chacun des autres.
2. ✏️ Combien de câbles sont nécessaires pour relier ces 9 ordinateurs ?
3. ✏️ Généraliser pour $n$ ordinateurs.
3. ✏️ Le câble entre A et B est coupé. Donner deux exemples de chemins qui peuvent être utilisés pour envoyer un message entre A et B.
4. ✏️ Triche : Un ordinateur contrôle-t-il l'ensemble des communications, comme dans les exemples précédents ?
5. ✏️ Panne : Si un ordinateur tombe en panne, cela affecte-t-il les communications entre les autres ordinateurs ?

### 4. Réseau acentré

Dans cet exemples, les joueuses commencent par constituer des petits réseaux, au sein desquels tous les ordinateurs sont connectés à la plupart des autres : le premier groupe est A, B, C, le deuxième est D, E, F, le troisième est G, H, I. Puis ces trois réseaux sont connectés entre eux, en reliant entre eux des ordinateurs de chaque petit réseau.

```mermaid
graph LR
	A --- D
	A --- G
	C --- H
	D --- G
	F --- H
	subgraph premier
    A --- B
		A --- C
		B --- C
  end
  subgraph deuxième
		D --- E
		E --- F
	end
	subgraph troisième
		G --- I
		H --- I
	end
```

#### Questions

1. ✏️ Combien de câbles sont nécessaires ?
2. ✏️ Le câble entre A et D, et celui entre A et B, sont coupés. Donner un chemin possible pour envoyer un message de B à F.
3. ✏️ Triche : Un ordinateur contrôle-t-il l'ensemble des communications, comme dans les exemples précédents ?
4. ✏️ Panne : Si un ordinateur tombe en panne, cela affecte-t-il les communications entre les autres ordinateurs ?

## 5. Bilan

1. ✏️ Quels sont les avantages / inconvénients de chaque type de réseau ? Pour répondre à cette question, complétez le tableau suivant :

| Type  | Avantages  | Inconvénients |
| :--: | :-- | :-- |
| Centralisé | Peu de cables, connexions nécessaires. Pour $`n`$ hôtes, il y a $`n - 1`$ connexions. | Peu tolérant aux pannes, triches et espionnages faciles pour l'hôte central. |
| Décentralisé | | |
| Complet | | |
| Acentré | | |

2. ✏️ Parmi les types de réseau étudiés, à quel type appartient l'Internet ?

## 6. Pour aller plus loin : Peut-on couper l'internet ?

✏️ Pour répondre à cette question cherchez des événements (date, lieu, circonstances ) où l'Internet a été coupé et complétez le tableau suivant : 

| Date | Lieu (Pays, région) | Description des circonstances | Url de l'article |
| :--: | :--: | :-- | :--: |
| | | | |
| | | | |
| | | | |

---
Source :  Licence CC-BY-SA - Paternault Louis