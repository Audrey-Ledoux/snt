**La Fabrique des Communs Pédagogiques** est un incubateur francophone à communs (ressources, communautés) dans l'éducation et la recherche. Elle agit dans un cadre d’action complémentaire et alternatif aux dispositifs actuels de soutien et aux dynamiques existantes dans le champ de l’éducation. Elle se concentre à faire émerger des coalitions d’acteurs qui sont dans la nécessité de co-construire des ressources en commun pour massifier et accélérer des projets éducatifs.

J'ai été contactée par la Fabrique des Communs Pédagogiques pour évoquer **l'histoire de sacado.xyz** et de notre équipe d'enseignants qui sont un exemple de **détermination, de travail, de positivisme et d'esprit d'entreprise**, mais aussi l'évolution du projet et le développement de la fraction en sciences physiques et chimie dont je suis responsable. Nous engageons aussi une discussion sur le sujet crucial **des femmes dans le numérique** et les challenges éducatifs associés qui passent en grande partie selon moi par **l'exemple et l'exemplarité**.

**MON INTERVIEW ici :** \\
https://fabpeda.org/2022/09/06/audrey-ledoux-sacado-un-outil-pedagogique-pense-par-des-enseignants-et-pour-lenseignement/
