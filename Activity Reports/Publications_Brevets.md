Vous trouverez aux liens suivant les publications scientifiques et les brevets issues de mes recherches en Master et Doctorat : 

**Brevet** 

https://patents.google.com/patent/FR3003571B1/fr



**Publications** 

https://pubs.rsc.org/en/content/articlelanding/2015/GC/C5GC00417A

https://onlinelibrary.wiley.com/doi/epdf/10.1002/anie.201508395

https://onlinelibrary.wiley.com/doi/epdf/10.1002/anie.201904898

https://pubs.rsc.org/en/content/articlelanding/2015/ra/c5ra14309h

