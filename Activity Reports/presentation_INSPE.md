Photos réalisées gracieusement par Olivier Anrigo, photographe presse et animalier [@OlivierAnrigo](https://www.instagram.com/olivieranrigo/?hl=fr) 



![Présentation et formation sacado.xyz à l'INSPE : introduction](./assets/Audrey_sacado_INSPE_OlivierAnrigo-1.jpg)

Présentation et formation sacado.xyz à l'INSPE : introduction


![Présentation et formation sacado.xyz à l'INSPE : présentation de la plateforme](./assets/Audrey_sacado_INSPE_OlivierAnrigo-5.jpg)

Présentation et formation sacado.xyz à l'INSPE : présentation de la plateforme


![Présentation et formation sacado.xyz à l'INSPE : exemples d'exercices](./assets/Audrey_sacado_INSPE_OlivierAnrigo-3.jpg)

Présentation et formation sacado.xyz à l'INSPE : exemples d'exercices
