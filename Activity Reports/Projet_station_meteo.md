Projet à champs d'action pluridisciplinaire, interdisciplinaire en deux phases.

- Phase 1 : Construction de la station météo avec transmission de notion (simples) de programmation, d'architecture réseaux, de capatation de données (physique), transmission de données (informatique). Exploitable en intégralité dans un fabLab ou club codding. Exploitable partiellement avec contextualisation en cours dans les cadres des programmes du B.O.

- Phase 2 : Exploitation pédagogique des données dans le cadre d'une étude et sensibilisation des paramètres environnementaux du bassin méditerranéen. Perseptive de construction d'un petit réseau de stations sur le bassin.


Notions intervenantes en Sciences et Informatique : 

- Arduino, Python, Raspberry, VNC viewer, serveur MQTT, Javascript, format de données JSON, Opendata
- Environnement, paramètres physiques environnementaux, mannipulation de données


![Présentation projet station meteo](./assets/Projet_inspe.pdf)




