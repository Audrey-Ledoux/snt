# Partie 1 : Présentation de l'expérience à misterX

Regardons la première partie de la vidéo dans laquelle les informaticiens expliquent comment ils vont suivre la vie de MisterX.

min  13:22   : <a href= "https://www.youtube.com/watch?v=djbwzEIv7gE">https://www.youtube.com/watch?v=djbwzEIv7gE</a>

### Questions : 

1. Quel dispositif les informaticiens vont-ils utliser pour suivre la vie de misterX ?

2. Quelles informations vont-ils prélever ?

3. Selon-vous qu'est-ce qu'une métadonnée ?


# Partie 2 : Activité d'investigation : Extraction de métadonnées

Consigne  : A partir des sites fournis ci-dessous : retrouver le lieu EXACT d'où cette photo a été prise ?

![](./assets/ExempleDidier.jpg)

Site 1 : <a target="_blank" href="http://metapicz.com/#landing"> http://metapicz.com/#landing </a>

Site 2 : <a target="_blank" href="https://www.openstreetmap.org/#map=6/33.971/9.562"> https://www.openstreetmap.org/#map=6/33.971/9.562 </a>

### Comprendre la géolocalisation

Regardons cette vidéo <a href="https://www.youtube.com/watch?v=iTfNhcC2vBA"> https://www.youtube.com/watch?v=iTfNhcC2vBA </a>

Animation lattitude et de longitude : <a href="https://www.geogebra.org/m/jz57pjrp"> https://www.geogebra.org/m/jz57pjrp </a>


# Partie 3 : Résultat de l'expérience sur misterX

Regardons la deuxième partie de la vidéo dans laquelle les informaticiens montrent à misterX les résultats de l'exploitation de ses métadonnées.

min 48:10 à 55:20    : <a href= "https://www.youtube.com/watch?v=djbwzEIv7gE">https://www.youtube.com/watch?v=djbwzEIv7gE</a>

### Questions : 

1. Donner des exemples de métadonnées. Sous quel format ?

2. Quelles sont les informations que les informaticiens ont pu extraire ? Quelle est finalement la réaction de misterX ?


### Activité de recherche : Algorithmique

Consigne : 

__Situation 1__ : Vous etes informaticien chez facebook. Proposez un algorithme de traitement de des données de misterX pour lui envoyer des publicités au meilleur moemnt de sa journée.

__Situation 2__ : Vous etes informaticien chez interpol. Proposez un algorithme de traitement de des données d'un activiste politique afin de pouvoir l'intercepter de manière certaine ?


# Bilan

Une métadonnée est une .............servant à .............. une autre .................

Le format d'un fichier de métadonnées est le plus souvent .............. mais aussi ................

On stocke les donnée dans des tableaux indexés appelés .................................

Un algorithme est une suite précise d'.................... et d'...................... permettant de résoudre systématiquement un .............................

# Quizz évaluation

Rendez-vous sur  <a href="https://sacado.xyz/group/d2eccae5"> https://sacado.xyz/group/d2eccae5 </a>
Créez votre compte et répondez au quizz que vous trouverez.

# Le monde de demain...

Recherche sur le web et argumentation à l'oral :

__Sujet 1__ : Chercher des soltutions sur internet pour limitter la transmission des données personnelles (moteurs de recherche, chats, réseaux sociaux)

__Sujet 2__ : Estimez le cout énergétique et environnemental du fonctionnement des grands serveurs de stockage de données.

